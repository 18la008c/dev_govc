FROM ubuntu:22.04

USER root

RUN apt-get update && apt-get install -y \
    curl \
    jq

RUN curl -L -o - "https://github.com/vmware/govmomi/releases/latest/download/govc_$(uname -s)_$(uname -m).tar.gz" | tar -C /usr/local/bin -xvzf - govc

WORKDIR /app/govc
